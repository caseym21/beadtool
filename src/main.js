var button = document.getElementById("btn");

button.addEventListener("click", function(){
    const total_length = document.querySelector(".container").querySelector("#total").value;
    const side_length = document.getElementById("side-length").value;
    const hanger_length = document.getElementById("hanger-length").value;
    const hanger_count = document.getElementById("hanger-count").value - 0; // No idea. Doesn't work if I don't - 0.
    const empty_hanger_length = ((total_length - side_length*2) - (hanger_count*hanger_length))/(hanger_count - 1 + 2);
    document.getElementById("error").innerHTML = ""; //Clears the error div if the next calculation doesn't contain decimals

    loop_output = "";
    if(empty_hanger_length%1 != 0){
        document.getElementById("error").innerHTML = `<p> The empty tabs in the middle have an unequal/fractional amount of space allotted to them.
        <br> You have an extra ${((empty_hanger_length - Math.floor(empty_hanger_length)) * (hanger_count + 1)).toFixed(0)} empty spaces to allot for 
        <br> This can be done by distributing ${((empty_hanger_length - Math.floor(empty_hanger_length)) * (hanger_count + 1)).toFixed(0)} beads between the middle hangers or 
        <br> an extra ${((empty_hanger_length - Math.floor(empty_hanger_length)) * (hanger_count + 1)).toFixed(0)} empty spaces to one middle empty hanger</p>`;
    }
    
    for(i = 0; i < (hanger_count*2 + 2); i++){
        if(i === 0){
            loop_output += "place " + side_length + " beads, ";
        } else if(i%2 === 0){
            loop_output += "place " + hanger_length + " beads, ";
        } else{
            loop_output += "leave " + Math.floor(empty_hanger_length) + ", ";
        }
    }
    loop_output += "place " + side_length + " beads";    
    document.getElementById("output").innerHTML = loop_output;
});